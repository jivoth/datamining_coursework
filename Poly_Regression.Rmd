---
title: "Midterm 1"
author: "Jonathan Voth"
output:
  html_document:
    df_print: paged
---

```{r}
source("/Users/JonathanVoth/Desktop/R Codes/myfunctions.R")
House_electricity <- read.csv("~/Desktop/Data Sets/House_electricity.csv")

# Exploratory plots
par(mfrow = c(1,2))
plot(KW.Hrs.Mnth ~ Family.Size, data = House_electricity)
plot(KW.Hrs.Mnth ~ Home.Size, data = House_electricity)
```


```{r}
RNGkind(sample.kind = "Rounding")
set.seed(0) # Setting the seed
p3 <- partition.3(House_electricity, 0.6, 0.3) # Creating 60:30:10 partition
training.data <- p3$data.train
validation.data <- p3$data.val
test.data <- p3$data.test

num <- 5
rmse.train <- rep(NA, num)
rmse.validation <- rep(NA, num)

# For loop to look at different degrees
for (q in 1:num){
  # Fit polynomial model on training data
  poly.train <- lm(KW.Hrs.Mnth ~ Family.Size + poly(Home.Size, degree = q), data = training.data)
  # RMSE for training data
  error.train <- poly.train$fitted.values - training.data$KW.Hrs.Mnth
  rmse.train[q] <- sqrt(mean(error.train^2))
  
  # Prediction on validation data
  yhat <- predict(poly.train, newdata = data.frame(validation.data))
  # RMSE for validation data
  error.validation <- yhat - validation.data$KW.Hrs.Mnth
  rmse.validation[q] <- sqrt(mean(error.validation^2))
}

# Plotting the RMSE values with different degrees
plot(c(1,5), c(160,240), type = "n", xlab = "Degree", ylab = "RMSE")
lines(seq(1,5), rmse.train, col = "blue")
lines(seq(1,5), rmse.validation, col = "red")
legend("topright", legend = c("Training", "Validation"), col = c("blue", "red"), lwd = 2)
```
I chose 3 as the optimal degree because there was a big drop-off in the RMSE value from a degree of 2 to 3. The same can be seen in the validation data.
```{r}
# Using 3 as optimal degree
# Fit polynomial model with optimal degree on training data
training.data.all <- rbind(training.data, validation.data)
final.model <- lm(KW.Hrs.Mnth ~ Family.Size + poly(Home.Size, degree = 3), data = training.data.all)
summary(final.model)

# Prediction on test data
yhat <- predict(final.model, newdata=data.frame(test.data))
# RMSE for test data
error.test <- yhat - test.data$KW.Hrs.Mnth
rmse.test <- sqrt(mean(error.test^2))
rmse.test
```


```{r}
